package fr.incontournables.followdev.model

import fr.incontournables.followdev.entity.Training
import org.jetbrains.anko.db.*

class TrainingDb(private val dbHelper: FollowDevDbHelper) {

    fun getTrainings() = dbHelper.use {
        select(TrainingTable.NAME,
            TrainingTable.ID,
            TrainingTable.TITLE,
            TrainingTable.DESCRIPTION,
            TrainingTable.TYPE,
            TrainingTable.BREAKS,
            TrainingTable.SERIES,
            TrainingTable.REPETITIONS,
            TrainingTable.WEIGHT,
            TrainingTable.GOAL
        ).parseList(classParser<Training>())
    }

    fun getTraining(training_id: Int) = dbHelper.use {
        select(TrainingTable.NAME,
            TrainingTable.ID,
            TrainingTable.TITLE,
            TrainingTable.DESCRIPTION,
            TrainingTable.TYPE,
            TrainingTable.BREAKS,
            TrainingTable.SERIES,
            TrainingTable.REPETITIONS,
            TrainingTable.WEIGHT,
            TrainingTable.GOAL
        ).whereArgs("training_id = {trainingId}",
            "trainingId" to training_id
        ).parseSingle(classParser<Training>())
    }

    fun insertTraining(training: Training) = dbHelper.use {
        insert(TrainingTable.NAME,
            TrainingTable.TITLE to training.title,
            TrainingTable.DESCRIPTION to training.description,
            TrainingTable.TYPE to training.type,
            TrainingTable.BREAKS to training.breaks,
            TrainingTable.SERIES to training.series,
            TrainingTable.REPETITIONS to training.repetitions,
            TrainingTable.WEIGHT to training.weight,
            TrainingTable.GOAL to training.goal
        )
    }

    fun updateTraining(training: Training) = dbHelper.use {
        update(TrainingTable.NAME,
            TrainingTable.TITLE to training.title,
            TrainingTable.DESCRIPTION to training.description,
            TrainingTable.TYPE to training.type,
            TrainingTable.BREAKS to training.breaks,
            TrainingTable.SERIES to training.series,
            TrainingTable.REPETITIONS to training.repetitions,
            TrainingTable.WEIGHT to training.weight,
            TrainingTable.GOAL to training.goal
        ).whereArgs("training_id = {trainingId}",
                "trainingId" to training.training_id
        ).exec()
    }

    fun deleteTraining(training_id: Int) = dbHelper.use {
        delete(TrainingTable.NAME,
            "training_id = {trainingId}",
            "trainingId" to training_id
        )
    }
}