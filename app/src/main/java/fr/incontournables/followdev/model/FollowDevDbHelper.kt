package fr.incontournables.followdev.model

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import org.jetbrains.anko.db.*

class FollowDevDbHelper(ctx: Context) : ManagedSQLiteOpenHelper(ctx,
    DB_NAME, null, DB_VERSION) {

    companion object {
        val DB_NAME = "followdev.db"
        val DB_VERSION = 1
    }

    override fun onCreate(db: SQLiteDatabase) {
        db.createTable(TrainingTable.NAME,
                true,
                TrainingTable.ID to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
                TrainingTable.TITLE to TEXT,
                TrainingTable.DESCRIPTION to TEXT,
                TrainingTable.TYPE to TEXT,
                TrainingTable.BREAKS to INTEGER,
                TrainingTable.SERIES to INTEGER,
                TrainingTable.REPETITIONS to INTEGER,
                TrainingTable.WEIGHT to INTEGER,
                TrainingTable.GOAL to TEXT
        )

        db.createTable(PerformanceTable.NAME,
                true,
                PerformanceTable.ID to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
                PerformanceTable.BREAKS to INTEGER,
                PerformanceTable.SERIES to INTEGER,
                PerformanceTable.REPETITIONS to INTEGER,
                PerformanceTable.WEIGHT to INTEGER,
                PerformanceTable.TRAINING_ID to INTEGER,
                FOREIGN_KEY(PerformanceTable.TRAINING_ID, TrainingTable.NAME, TrainingTable.ID)
        )

        db.insert(TrainingTable.NAME,
                TrainingTable.TITLE to "Wide Chest Press",
                TrainingTable.DESCRIPTION to "Le Wide Chest Press cible les muscles des pectoraux comme : Le grand pectoral, les muscles deltoïdes antérieurs (clavicules) et également les triceps. La musculation des pectoraux est très populaire chez les pratiquants de la musculation ; la presse à pecs est alors idéale pour les muscler, les redessiner et prendre du volume.",
                TrainingTable.TYPE to "muscle",
                TrainingTable.BREAKS to 60,
                TrainingTable.SERIES to 3,
                TrainingTable.REPETITIONS to 10,
                TrainingTable.WEIGHT to 0,
                TrainingTable.GOAL to "60/3/10/0"
        )

        db.insert(TrainingTable.NAME,
                TrainingTable.TITLE to "Low Row",
                TrainingTable.DESCRIPTION to " Avec le Low Row vous travaillerez les muscles de votre dos comme : le rhomboïdes, les dorsaux, et les trapèzes. Vous exercerez inévitablement vos bras avec les biceps et les triceps.",
                TrainingTable.TYPE to "muscle",
                TrainingTable.BREAKS to 60,
                TrainingTable.SERIES to 3,
                TrainingTable.REPETITIONS to 10,
                TrainingTable.WEIGHT to 0,
                TrainingTable.GOAL to "60/3/10/0"
        )

        db.insert(TrainingTable.NAME,
                TrainingTable.TITLE to "Schoulder Press",
                TrainingTable.DESCRIPTION to "La Shoulder Press renforce vos épaules avec notamment les deltoïdes, et les bras en ciblant plus particulièrement les triceps. La Presse à Épaule permet de réaliser des exercices très efficaces afin de développer et tonifier ses épaules et obtenir une jolie forme arrondie.",
                TrainingTable.TYPE to "muscle",
                TrainingTable.BREAKS to 60,
                TrainingTable.SERIES to 3,
                TrainingTable.REPETITIONS to 10,
                TrainingTable.WEIGHT to 0,
                TrainingTable.GOAL to "60/3/10/0"
        )

        db.insert(TrainingTable.NAME,
                TrainingTable.TITLE to " Chest Press",
                TrainingTable.DESCRIPTION to "Le Chest Press va solliciter majoritairement les pectoraux, les deltoïdes et les triceps.",
                TrainingTable.TYPE to "muscle",
                TrainingTable.BREAKS to 60,
                TrainingTable.SERIES to 3,
                TrainingTable.REPETITIONS to 10,
                TrainingTable.WEIGHT to 0,
                TrainingTable.GOAL to "60/3/10/0"
        )

        db.insert(TrainingTable.NAME,
                TrainingTable.TITLE to "Arm Extension",
                TrainingTable.DESCRIPTION to "L’Arm Extension est une machine de musculation ciblant spécifiquement vos Triceps. Elle permettra également d’accroître la force de vos bras, pour gagner en volume et en tonicité.",
                TrainingTable.TYPE to "muscle",
                TrainingTable.BREAKS to 60,
                TrainingTable.SERIES to 3,
                TrainingTable.REPETITIONS to 10,
                TrainingTable.WEIGHT to 0,
                TrainingTable.GOAL to "60/3/10/0"
        )
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        db.dropTable(TrainingTable.NAME, true)
        db.dropTable(PerformanceTable.NAME, true)
        onCreate(db)
    }
}