package fr.incontournables.followdev.model

object TrainingTable {
    val NAME = "trainings"
    val ID = "training_id"
    val TITLE = "title"
    val DESCRIPTION = "description"
    val TYPE = "type"
    val BREAKS = "breaks"
    val SERIES = "series"
    val REPETITIONS = "repetitions"
    val WEIGHT = "weight"
    val GOAL = "goal"
}

object PerformanceTable {
    val NAME = "performances"
    val ID = "performance_id"
    val BREAKS = "breaks"
    val SERIES = "series"
    val REPETITIONS = "repetitions"
    val WEIGHT = "weight"
    val TRAINING_ID = "training_id"
}
