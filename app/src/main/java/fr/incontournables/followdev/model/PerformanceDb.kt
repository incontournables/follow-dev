package fr.incontournables.followdev.model

import fr.incontournables.followdev.entity.Performance
import org.jetbrains.anko.db.*

class PerformanceDb(private val dbHelper: FollowDevDbHelper) {

    fun getPerformances(training_id: Int) = dbHelper.use {
        select(PerformanceTable.NAME,
                PerformanceTable.ID,
                PerformanceTable.BREAKS,
                PerformanceTable.SERIES,
                PerformanceTable.REPETITIONS,
                PerformanceTable.WEIGHT,
                PerformanceTable.TRAINING_ID
        ).whereArgs("training_id = {trainingId}",
                "trainingId" to training_id
        ).parseList(classParser<Performance>())
    }

    fun insertPerformance(performance: Performance) = dbHelper.use {
        insert(PerformanceTable.NAME,
                PerformanceTable.BREAKS to performance.breaks,
                PerformanceTable.SERIES to performance.series,
                PerformanceTable.REPETITIONS to performance.repetitions,
                PerformanceTable.WEIGHT to performance.weight,
                PerformanceTable.TRAINING_ID to performance.training_id
        )
    }

    fun updatePerformance(performance: Performance) = dbHelper.use {
        update(PerformanceTable.NAME,
                PerformanceTable.BREAKS to performance.breaks,
                PerformanceTable.SERIES to performance.series,
                PerformanceTable.REPETITIONS to performance.repetitions,
                PerformanceTable.WEIGHT to performance.weight
        ).whereArgs("performance_id = {performanceId}",
                "performanceId" to performance.performance_id
        ).exec()
    }

    fun deletePerformance(training_id: Int) = dbHelper.use {
        delete(PerformanceTable.NAME,
                "training_id = {trainingId}",
                "trainingId" to training_id
        )
    }

    fun getLastPerformance(training_id: Int) = dbHelper.use {
        select(PerformanceTable.NAME,
                "MAX(${PerformanceTable.ID}) as performance_id",
                PerformanceTable.WEIGHT,
        ).whereArgs( "training_id = {trainingId}",
                "trainingId" to training_id
        ).parseOpt(object: MapRowParser<Array<String>> {
            override fun parseRow(columns: Map<String, Any?>): Array<String> {
                val data = Array<String>(1) {
                    it -> columns.getValue("weight").toString()
                }
                return data;
            }
        })
    }
}