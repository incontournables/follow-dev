package fr.incontournables.followdev.entity

data class Training(
    val training_id: Int,
    val title: String,
    val description: String,
    val type: String,
    val breaks: Int,
    val series: Int,
    val repetitions: Int,
    val weight: Int,
    val goal: String
)
