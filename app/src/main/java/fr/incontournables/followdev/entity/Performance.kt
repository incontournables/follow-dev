package fr.incontournables.followdev.entity

data class Performance(
    val performance_id: Int,
    val breaks: Int,
    val series: Int,
    val repetitions: Int,
    val weight: Int,
    val training_id: Int
)
