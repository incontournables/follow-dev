package fr.incontournables.followdev.controller

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import fr.incontournables.followdev.MainActivity
import fr.incontournables.followdev.R
import fr.incontournables.followdev.model.FollowDevDbHelper
import fr.incontournables.followdev.model.TrainingDb
import kotlinx.android.synthetic.main.activity_training_list_details.*
import kotlinx.android.synthetic.main.activity_training_list_details.changeMode

class TrainingListDetailsActivity : AppCompatActivity() {

    val trainingDb by lazy { TrainingDb(FollowDevDbHelper(applicationContext)) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_training_list_details)
    }

    override fun onResume() {
        super.onResume()
        // Get all trainings
        val trainingsArray = trainingDb.getTrainings()
        trainingListDetailsRecyclerView.layoutManager = LinearLayoutManager(this)
        trainingListDetailsRecyclerView.adapter = TrainingListDetailsAdapter(trainingsArray.toTypedArray())

        // Change mode / activity on button click
        changeMode.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
    }

    @SuppressLint("ResourceType")
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        // Display menu
        menuInflater.inflate(R.layout.navigation, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_activities -> { // Activity with simple activities
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
            }
            R.id.action_decathlon -> { // Activity with Decathlon activities
                val intent = Intent(this, TrainingDecathlonActivity::class.java)
                startActivity(intent)
            }
            R.id.action_add_activities -> {
                val intent = Intent(this, AddTrainningActivity::class.java)
                startActivity(intent)
            }
        }
        return super.onOptionsItemSelected(item)
    }
}