package fr.incontournables.followdev.controller

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import fr.incontournables.followdev.MainActivity
import fr.incontournables.followdev.R
import fr.incontournables.followdev.entity.Training
import fr.incontournables.followdev.model.FollowDevDbHelper
import fr.incontournables.followdev.model.PerformanceDb
import fr.incontournables.followdev.model.TrainingDb
import kotlinx.android.synthetic.main.activity_training_detail.*

class TrainingDetailActivity : AppCompatActivity() {

    val trainingDb by lazy { TrainingDb(FollowDevDbHelper(applicationContext)) }
    val performanceDb by lazy { PerformanceDb(FollowDevDbHelper(applicationContext)) }
    var trainingId: Int = 0
    lateinit var training: Training

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_training_detail)
    }

    override fun onResume() {
        super.onResume()
        // Get training id from another activity
        trainingId = intent.getIntExtra("trainingId", 0)
        // Get training
        training = trainingDb.getTraining(trainingId)
        // Set data
        trainingTitle.text = training.title
        trainingDescription.text = training.description
        trainingGoal.text = training.goal
        trainingSeries.text = training.series.toString()
        trainingWeight.text= training.weight.toString()
        trainingBreaks.text = training.breaks.toString()
        trainingRepetitions.text = training.repetitions.toString()

        // Change activity to add performance
        addPerf.setOnClickListener {
            val intent = Intent(this, PerformanceFormActivity::class.java)
            intent.putExtra("trainingId", trainingId)
            startActivity(intent)
        }
        // Change activity to display performances
        showPerfs.setOnClickListener {
            val intent = Intent(this, TrainingPerformancesActivity::class.java)
            intent.putExtra("trainingId", trainingId)
            startActivity(intent)
        }
        // Change activity to update training
        updateTraining.setOnClickListener {
            val intent = Intent(this,TrainingUpdateActivity::class.java)
            intent.putExtra("trainingId", trainingId)
            intent.putExtra("title", training.title)
            intent.putExtra("description", training.description)
            intent.putExtra("type", training.type)
            intent.putExtra("goal", training.goal)
            intent.putExtra("weight", training.weight.toString())
            intent.putExtra("repetitions", training.repetitions.toString())
            intent.putExtra("breaks", training.breaks.toString())
            intent.putExtra("series", training.series.toString())
            startActivity(intent)
        }
        // Change activity after delete training
        deleteTraining.setOnClickListener {
            trainingDb.deleteTraining(trainingId)
            performanceDb.deletePerformance(trainingId)
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    @SuppressLint("ResourceType")
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        // Display menu
        menuInflater.inflate(R.layout.navigation, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_activities -> { // Activity with simple activities
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
            }
            R.id.action_decathlon -> { // Activity with Decathlon activities
                val intent = Intent(this, TrainingDecathlonActivity::class.java)
                startActivity(intent)
            }
            R.id.action_add_activities -> {
                val intent = Intent(this, AddTrainningActivity::class.java)
                startActivity(intent)
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
