package fr.incontournables.followdev.controller

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import fr.incontournables.followdev.R
import fr.incontournables.followdev.entity.Training
import fr.incontournables.followdev.model.PerformanceDb
import kotlinx.android.synthetic.main.training_of_list.view.*

class TrainingAdapter (val items: Array<Training>, val performanceDb: PerformanceDb, val clickListner: OnItemClickListner) : RecyclerView.Adapter<TrainingAdapter.ViewHolder>() {

    private val TAG = "MyActivity"

    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view){
        fun bindTraining( training : Training, performanceDb: PerformanceDb){
            with(training){
                val perf = performanceDb.getLastPerformance(training_id)
                val perfWeight = if (perf?.get(0) == "null") "Aucune" else "${perf?.get(0)} Kg"
                val strs = splitToArray(goal)
                itemView.trainingTitle.text = title
                itemView.trainingWeight.text = "Dernière performance : ${perfWeight}"
                itemView.id_goal_rep.text = strs[2]
                itemView.id_goal_repos.text = strs[0]
                itemView.id_goal_series.text = strs[1]
                itemView.id_goal_weight.text = strs[3]
            }
        }

        fun initialize(item: Training, action: OnItemClickListner) {
            itemView.setOnClickListener {
                action.OnItemClick(item, adapterPosition)
            }
        }

        private fun splitToArray(string: String): Array<String> {
            return string.split("/").toTypedArray()
        }
    }

    override fun getItemCount(): Int = items.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val lineView =
            LayoutInflater.from(parent.context).inflate(R.layout.training_of_list, parent, false)
        return ViewHolder(lineView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindTraining(items[position], performanceDb)
        holder.initialize(items[position],clickListner)
    }

}

interface OnItemClickListner {
    fun OnItemClick(item: Training, position: Int)
}