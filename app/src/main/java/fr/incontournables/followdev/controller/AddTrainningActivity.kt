package fr.incontournables.followdev.controller

import android.content.Intent
import android.os.Bundle
import android.widget.RadioButton
import android.widget.RadioGroup
import androidx.appcompat.app.AppCompatActivity
import fr.incontournables.followdev.MainActivity
import fr.incontournables.followdev.R
import fr.incontournables.followdev.entity.Training
import fr.incontournables.followdev.model.FollowDevDbHelper
import fr.incontournables.followdev.model.TrainingDb
import kotlinx.android.synthetic.main.activity_add_trainning.*

import kotlinx.android.synthetic.main.activity_training_update_form.goal_repetitions
import kotlinx.android.synthetic.main.activity_training_update_form.goal_repos
import kotlinx.android.synthetic.main.activity_training_update_form.goal_series
import kotlinx.android.synthetic.main.activity_training_update_form.goal_weight
import kotlinx.android.synthetic.main.activity_training_update_form.repetitions
import kotlinx.android.synthetic.main.activity_training_update_form.repos
import kotlinx.android.synthetic.main.activity_training_update_form.series
import kotlinx.android.synthetic.main.activity_training_update_form.weight

class AddTrainningActivity : AppCompatActivity() {
    var trainingId: Int = 0
    val trainingDb by lazy { TrainingDb(FollowDevDbHelper(applicationContext)) }
    var goal: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_trainning)
    }

    override fun onResume() {
        super.onResume()
        addSubmit.setOnClickListener {
            submitAdd()
        }
        updateCancel.setOnClickListener {
            cancelAdd()
        }
    }

    private fun cancelAdd() {
        // Return to home page
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }

    private fun submitAdd() {
        goal = "${goal_repetitions.text}/${goal_series.text}/${goal_repos.text}/${goal_weight.text}"
        val radioButton = findViewById<RadioButton>(radioGroup.checkedRadioButtonId)
        val training: Training = Training(
            trainingId,
            title_training.text.toString(),
            description_training.text.toString(),
            radioButton.text.toString(),
            repos.text.toString().toInt(),
            series.text.toString().toInt(),
            repetitions.text.toString().toInt(),
            weight.text.toString().toInt(),
            goal
        )
        trainingDb.insertTraining(training)
        cancelAdd()
    }
}