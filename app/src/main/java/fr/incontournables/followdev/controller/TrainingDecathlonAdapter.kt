package fr.incontournables.followdev.controller

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import fr.incontournables.followdev.R
import kotlinx.android.synthetic.main.training_decathlon_of_list.view.*

class TrainingDecathlonAdapter (val items: Array<String>) : RecyclerView.Adapter<TrainingDecathlonAdapter.ViewHolder>() {

    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        fun bindTrainingDecathlon(trainingDecathlon: String) {
            itemView.trainingDecathlonTitle.text = trainingDecathlon
        }
    }

    override fun getItemCount(): Int = items.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val lineView = LayoutInflater.from(parent.context).inflate(R.layout.training_decathlon_of_list, parent, false)
        return ViewHolder(lineView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindTrainingDecathlon(items[position])
    }
}
