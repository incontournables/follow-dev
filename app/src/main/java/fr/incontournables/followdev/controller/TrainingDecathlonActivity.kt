package fr.incontournables.followdev.controller

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.os.StrictMode
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import fr.incontournables.followdev.MainActivity
import fr.incontournables.followdev.R
import kotlinx.android.synthetic.main.activity_training_decathlon.*
import okhttp3.*
import org.json.JSONObject
import java.io.IOException
import java.util.*

class TrainingDecathlonActivity : AppCompatActivity() {

    private val client = OkHttpClient()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_training_decathlon)
        // Permit all to curl API easier
        StrictMode.setThreadPolicy(StrictMode.ThreadPolicy.Builder().permitAll().build())
    }

    override fun onResume() {
        super.onResume()
        // Get all trainings proposed by Decathlon API
        val trainingsDecathlonArray = curlApi()
        trainingDecathlonRecyclerView.layoutManager = LinearLayoutManager(this)
        trainingDecathlonRecyclerView.adapter = TrainingDecathlonAdapter(trainingsDecathlonArray)
    }

    @SuppressLint("ResourceType")
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        // Display menu
        menuInflater.inflate(R.layout.navigation, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_activities -> { // Activity with simple activities
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
            }
            R.id.action_decathlon -> { // Activity with Decathlon activities
                val intent = Intent(this, TrainingDecathlonActivity::class.java)
                startActivity(intent)
            }
            R.id.action_add_activities -> {
                val intent = Intent(this, AddTrainningActivity::class.java)
                startActivity(intent)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    fun curlApi() : Array<String> {
        // Prepare request
        val request = Request.Builder().url("https://sports.api.decathlon.com/sports/351").build()
        var trainings: Array<String>
        var responses: Response? = null

        try {
            // Get response if success
            responses = client.newCall(request).execute()
        } catch (e: IOException) {
            e.printStackTrace()
        }

        // Get only array with sports
        val jsonData = responses?.body()?.string()
        val jsonObject = JSONObject(jsonData)
        val data = jsonObject.getJSONObject("data");
        val relationships = data.getJSONObject("relationships")
        val tags = relationships.getJSONObject("tags")
        val newData = tags.getJSONArray("data");

        // Replace all '-' by space and set first letter as uppercase
        trainings = Array(newData.length()) {
            newData.getString(it).replace('-', ' ').capitalize(Locale.ROOT)
        }

        return trainings
    }
}