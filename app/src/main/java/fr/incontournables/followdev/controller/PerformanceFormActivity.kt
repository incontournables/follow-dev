package fr.incontournables.followdev.controller

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import fr.incontournables.followdev.R
import fr.incontournables.followdev.entity.Performance
import fr.incontournables.followdev.model.FollowDevDbHelper
import fr.incontournables.followdev.model.PerformanceDb
import kotlinx.android.synthetic.main.activity_form_performance.*

class PerformanceFormActivity : AppCompatActivity() {

    var trainingId: Int = 0
    val performanceDb by lazy { PerformanceDb(FollowDevDbHelper(applicationContext)) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_form_performance)
        // Get activity id
        trainingId = intent.getIntExtra("trainingId", 0)
    }

    override fun onResume() {
        super.onResume()
        // Add new performance
        perfSubmit.setOnClickListener {
            createPerformance()
        }
        // Cancel this form
        perfCancel.setOnClickListener {
            cancelPerformance()
        }
    }

    private fun createPerformance() {
        val performance: Performance = Performance(
                0,
                perfRepos.text.toString().toInt(),
                perfSeries.text.toString().toInt(),
                perfRepetitions.text.toString().toInt(),
                perfWeight.text.toString().toInt(),
                trainingId
        )
        // Create new performance for this training
        performanceDb.insertPerformance(performance)

        cancelPerformance()
    }

    private fun cancelPerformance() {
        // Return to previous activity
        val intent = Intent(this, TrainingDetailActivity::class.java)
        intent.putExtra("trainingId", trainingId)
        startActivity(intent)
        finish()
    }
}