package fr.incontournables.followdev.controller

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.github.aachartmodel.aainfographics.aachartcreator.AAChartModel
import com.github.aachartmodel.aainfographics.aachartcreator.AAChartType
import com.github.aachartmodel.aainfographics.aachartcreator.AAChartView
import com.github.aachartmodel.aainfographics.aachartcreator.AASeriesElement
import fr.incontournables.followdev.MainActivity
import fr.incontournables.followdev.R
import fr.incontournables.followdev.entity.Performance
import fr.incontournables.followdev.model.FollowDevDbHelper
import fr.incontournables.followdev.model.PerformanceDb
import kotlinx.android.synthetic.main.activity_performance_repetitions.*

class TrainingPerformancesActivity : AppCompatActivity() {

    val performanceDb by lazy { PerformanceDb(FollowDevDbHelper(applicationContext)) }
    lateinit var performances: List<Performance>
    lateinit var repetitions: Array<Any>
    lateinit var series: Array<Any>
    lateinit var breaks: Array<Any>
    lateinit var weights: Array<Any>
    var trainingId: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_performance_repetitions)
        // Get training id from another activity
        trainingId = intent.getIntExtra("trainingId", 0)
    }

    override fun onResume() {
        super.onResume()
        // Get performances
        performances = performanceDb.getPerformances(trainingId)
        // Get only performance repetitions
        repetitions = Array<Any>(performances.size) {
            i -> performances[i].repetitions
        }
        // Get only performance series
        series = Array<Any>(performances.size) {
            i -> performances[i].series
        }
        // Get only performance breaks
        breaks = Array<Any>(performances.size) {
            i -> performances[i].breaks
        }
        // Get only performance weights
        weights = Array<Any>(performances.size) {
            i -> performances[i].weight
        }
        displayChart()
    }

    @SuppressLint("ResourceType")
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        // Display menu
        menuInflater.inflate(R.layout.navigation, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_activities -> { // Activity with simple activities
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
            }
            R.id.action_decathlon -> { // Activity with Decathlon activities
                val intent = Intent(this, TrainingDecathlonActivity::class.java)
                startActivity(intent)
            }
            R.id.action_add_activities -> {
                val intent = Intent(this, AddTrainningActivity::class.java)
                startActivity(intent)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun displayChart() {
        val aaChartView = findViewById<AAChartView>(R.id.aa_chart_view)
        // Initialize chart
        val aaChartModel : AAChartModel = AAChartModel()
                .chartType(AAChartType.Area)
                .title("Performances")
                .subtitle("Performances")
                .backgroundColor("#E1E2E1")
                .dataLabelsEnabled(true)
                .colorsTheme(arrayOf("#ec7063", "#5dade2", "#82e0aa", "#f5b041"))
                .series(arrayOf(
                        AASeriesElement()
                                .name("Nombre de répétitions")
                                .data(repetitions),
                        AASeriesElement()
                                .name("Nombre de séries")
                                .data(series),
                        AASeriesElement()
                                .name("Temps de repos")
                                .data(breaks),
                        AASeriesElement()
                                .name("Poids")
                                .data(weights)
                ))
        // Draw previous chart
        aaChartView.aa_drawChartWithChartModel(aaChartModel)
    }
}