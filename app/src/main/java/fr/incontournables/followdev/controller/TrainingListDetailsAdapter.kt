package fr.incontournables.followdev.controller

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import fr.incontournables.followdev.R
import fr.incontournables.followdev.entity.Training
import kotlinx.android.synthetic.main.training_details_of_list.view.*

class TrainingListDetailsAdapter (val items: Array<Training>) : RecyclerView.Adapter<TrainingListDetailsAdapter.ViewHolder>() {

    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view){
        fun bindTraining(training: Training){
            with(training){
                itemView.trainingTitle.text = title
                itemView.trainingRepetitions.text = repetitions.toString()
                itemView.trainingSeries.text = series.toString()
                itemView.trainingRepos.text = breaks.toString()
                itemView.trainingPoids.text = weight.toString()
            }
        }
    }

    override fun getItemCount(): Int = items.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val lineView = LayoutInflater.from(parent.context).inflate(R.layout.training_details_of_list, parent, false)
        return ViewHolder(lineView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindTraining(items[position])
    }
}