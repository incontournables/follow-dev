package fr.incontournables.followdev.controller

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.widget.NumberPicker
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import fr.incontournables.followdev.R
import fr.incontournables.followdev.entity.Training
import fr.incontournables.followdev.model.FollowDevDbHelper
import fr.incontournables.followdev.model.TrainingDb
import kotlinx.android.synthetic.main.activity_training_update_form.*


class TrainingUpdateActivity : AppCompatActivity() {

    var trainingId: Int = 0
    val trainingDb by lazy { TrainingDb(FollowDevDbHelper(applicationContext)) }
    var goal: String = ""

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_training_update_form)
        // Get data
        goal = intent.getStringExtra("goal").toString()
        val goalArray = goal.split("/").toTypedArray()
        trainingId = intent.getIntExtra("trainingId", 0)
        title = intent.getStringExtra("title").toString()
        // Set form default values
        repos.setText(intent.getStringExtra("breaks"))
        repetitions.setText(intent.getStringExtra("repetitions"))
        weight.setText(intent.getStringExtra("weight"))
        series.setText(intent.getStringExtra("series"))
        goal_repetitions.setText(goalArray[0])
        goal_series.setText(goalArray[1])
        goal_repos.setText(goalArray[2])
        goal_weight.setText(goalArray[3])
    }

    override fun onResume() {
        super.onResume()
        updateCancel.setOnClickListener {
            cancelUpdate()
        }
        updateSubmit.setOnClickListener {
            updateTraining()
        }
    }

    private fun cancelUpdate() {
        // Return to previous activity
        val intent = Intent(this, TrainingDetailActivity::class.java)
        intent.putExtra("trainingId", trainingId)
        startActivity(intent)
        finish()
    }

    private  fun updateTraining() {
        goal = "${goal_repetitions.text}/${goal_series.text}/${goal_repos.text}/${goal_weight.text}"
        val training: Training = Training(
            trainingId,
            intent.getStringExtra("title").toString(),
            intent.getStringExtra("description").toString(),
            intent.getStringExtra("type").toString(),
            repos.text.toString().toInt(),
            series.text.toString().toInt(),
            repetitions.text.toString().toInt(),
            weight.text.toString().toInt(),
            goal
        )
        trainingDb.updateTraining(training)
        cancelUpdate()
    }
}