package fr.incontournables.followdev

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.KeyEvent
import android.view.Menu
import android.view.MenuItem
//Enlever le Toast
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import fr.incontournables.followdev.controller.*
import fr.incontournables.followdev.entity.Training
import fr.incontournables.followdev.model.FollowDevDbHelper
import fr.incontournables.followdev.model.PerformanceDb
import fr.incontournables.followdev.model.TrainingDb
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), OnItemClickListner {

    val trainingDb by lazy { TrainingDb(FollowDevDbHelper(applicationContext)) }
    val performanceDb by lazy { PerformanceDb(FollowDevDbHelper(applicationContext)) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onResume() {
        super.onResume()
        // Get all trainings
        val trainingsArray = trainingDb.getTrainings()
        // Display all activities
        trainingRecyclerView.layoutManager = LinearLayoutManager(this)
        trainingRecyclerView.adapter = TrainingAdapter(trainingsArray.toTypedArray(), performanceDb, this)
        // Change mode / activity on button click
        changeMode.setOnClickListener {
            val intent = Intent(this, TrainingListDetailsActivity::class.java)
            startActivity(intent)
        }
    }

    @SuppressLint("ResourceType")
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        // Display menu
        menuInflater.inflate(R.layout.navigation, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_activities -> { // Activity with simple activities
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
            }
            R.id.action_decathlon -> { // Activity with Decathlon activities
                val intent = Intent(this, TrainingDecathlonActivity::class.java)
                startActivity(intent)
            }
            R.id.action_add_activities -> {
                val intent = Intent(this, AddTrainningActivity::class.java)
                startActivity(intent)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun OnItemClick(item: Training, position: Int) {
        //Toast.makeText(this,item.description  , Toast.LENGTH_SHORT).show()
        val intent = Intent(this,TrainingDetailActivity::class.java)
        intent.putExtra("trainingId", item.training_id)
        startActivity(intent)
    }
}