# FollowDev

## Avant-propos

Ce projet a été réalisé par CASTANER Antony et ESPOSITO Dan.

## Objectifs

- Suivre l'évolution de son développement physique par l'intermédiaire d'une application minimaliste et simple d'utilisation,
- Avoir des objectifs clairs permettant de progresser à son rythme et de manière constructive et bénéfique pour la motivation

## Fonctionnalités

- Partie proposée par notre équipe :
    - Visualiser les listes d'activités,
    - Gérer une activité :
        - Ajouter,
        - Modifier,
        - Supprimer,
    - Fixer par activités des objectifs à atteindre,
    - Enregistrer ses performances,
    - Suivre ses performances à l'aide de tableaux et/ou graphiques.

- Partie proposée par l'API Décathlon :
    - Consultation d'activités.

## Pages

- Mes activités : liste des activités proposées par l'équipe
    - Chaque activité est représentée dans un bloc comportant :
        - Nom,
        - Dernière performance,
        - Objectif.
- Mes activités détaillées : liste des activités proposées et détaillées par l'équipe
    - Chaque activité est représentée dans un bloc plus détaillée comportant :
        - Nom,
        - Objectif,
        - Temps de repos,
        - Nombre de séries,
        - Nombre de répétitions,
        - Poids.
- Une activité : une page complète
    - Nom,
    - Description,
    - Objectif,
    - Temps de repos,
    - Nombre de séries,
    - Nombre de répétitions,
    - Poids,
    - Bouton 'Mes performances',
    - Bouton 'Ajouter une performance',
    - Bouton 'Modifier',
    - Bouton 'Supprimer'.
- Ajouter une activité : Formulaire vide
    - Description,
    - Objectif,
    - Temps de repos,
    - Nombre de séries,
    - Nombre de répétitions,
    - Poids.
- Modifier une activité : Formulaire pré-rempli
    - Description,
    - Objectif,
    - Temps de repos,
    - Nombre de séries,
    - Nombre de répétitions,
    - Poids.
- Mes performances : une page complète avec les performances réalisées pour une activité
    - Graphiques détaillés
- Ajouter une performance : Formulaire vide
    - Temps de repos,
    - Nombre de séries,
    - Nombre de répétitions,
    - Poids.
- Autres activités : liste des activités proposées par Decathlon.

## Activités proposées par l'équipe

- Nom : Wide Chest Press
    - Description :Le Wide Chest Press cible les muscles des pectoraux comme : Le grand pectoral, les muscles deltoïdes antérieurs (clavicules) et également les triceps. La musculation des pectoraux est très populaire chez les pratiquants de la musculation ; la presse à pecs est alors idéale pour les muscler, les redessiner et prendre du volume.,
    - Objectif = 60/3/10/0,
    - Temps de repos = 60,
    - Nombre de séries = 3,
    - Nombre de répétitions = 10,
    - Poids = 0
- Nom : Schoulder Press
    - Description : La Shoulder Press renforce vos épaules avec notamment les deltoïdes, et les bras en ciblant plus particulièrement les triceps. La Presse à Épaule permet de réaliser des exercices très efficaces afin de développer et tonifier ses épaules et obtenir une jolie forme arrondie.,
    - Objectif = 60/3/10/0,
    - Temps de repos = 60,
    - Nombre de séries = 3,
    - Nombre de répétitions = 10,
    - Poids = 0
- Nom : Low Row
    - Description : Avec le Low Row vous travaillerez les muscles de votre dos comme : le rhomboïdes, les dorsaux, et les trapèzes. Vous exercerez inévitablement vos bras avec les biceps et les triceps.,
    - Objectif = 60/3/10/0,
    - Temps de repos = 60,
    - Nombre de séries = 3,
    - Nombre de répétitions = 10,
    - Poids = 0
- Nom : Chest Press
    - Description : Le Chest Press va solliciter majoritairement les pectoraux, les deltoïdes et les triceps.,
    - Objectif = 60/3/10/0,
    - Temps de repos = 60,
    - Nombre de séries = 3,
    - Nombre de répétitions = 10,
    - Poids = 0
- Nom : Arm Extension
    - Description : L’Arm Extension est une machine de musculation ciblant spécifiquement vos Triceps. Elle permettra également d’accroître la force de vos bras, pour gagner en volume et en tonicité.,
    - Objectif = 60/3/10/0,
    - Temps de repos = 60,
    - Nombre de séries = 3,
    - Nombre de répétitions = 10,
    - Poids = 0

## Maquettes réalisées avant le développement

Liste des activités en mode aperçu

![Liste des activités en mode aperçu](docs/homepage.png)

Liste des activités en mode training

![Liste des activités en mode training](docs/activities_training.png)

Affichage d'une activité en détails

![Affichage d'une activité en détails](docs/activity_detail.png)

## Technique

### Dépendances et bibliothèques utilisées

- JetBrains Kotlin 1.4.10
- RecyclerView 1.1.0
- AndroidX Core 1.3.2
- AndroidX AppCompat 1.2.0
- AndroidX ConstraintLayout 2.0.4
- Android Material Design 1.2.1
- Anko SQLite 0.10.8
- OkHTTP 3.10.0
- AAChartModel (last version)

### Base de Données

#### Modèle Conceptuel de Données

![MCD](docs/MCD.PNG)

#### Schéma relationnel

**trainings** (<u>training_id</u>, title, description, type, breaks, series, repetitions, weight, goal)
**performances** (<u>performance_id</u>, breaks, series, repetitions, weight, #training_id)
